/* 
	Copyright (C) 2008, Brian Tanner

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.

	This code is adapted from the SampleMinesEnvironment.cpp from brian@tannerpages.com.
	
	*  $Revision: 996 $
	*  $Date: 2013-03-28 $
	*  $Author: georgios@pierris.gr $
	
*/

#include <stdio.h>   /*sprintf*/

#include <cassert>
#include <string>
#include <iostream>
#include <sstream>
#include <math.h>

// env_ function prototypes types 
#include <rlglue/Environment_common.h>	  

// helpful functions for allocating structs and cleaning them up 
#include <rlglue/utils/C/RLStruct_util.h> 

using namespace std; 

/* 	
	This is a very simple discrete-state, episodic grid world that has 
	exploding mines in it.  If the agent steps on a mine, the episode
	ends with a large negative reward.
	
	The reward per step is -1, with +10 for exiting the game successfully
	and -100 for stepping on a mine.
*/

#define WORLD_FREE 0
#define WORLD_OBSTACLE 1
#define WORLD_MINE 2 
#define WORLD_GOAL 3

/*
	world_description_t is a structure that holds all of the information
	about the world, including the placement of the mines and the position
	of the agent, etc.
*/
typedef struct 
{
  int numRows;		/* Number of rows in the world		*/
  int numCols;		/* Number of columns in the world	*/
  int agentRow;		/* Agent current row				*/
  int agentCol;		/* Agent current column				*/
  int orientation;  /* Agent's orientation				*/
} world_description_t;


// HELPER FUNCTIONS.  PROTOTYPES HERE, CODE AT THE BOTTOM OF FILE */

// Changes the (row,col) position of the agent into a scalar    
int calculate_flat_state(world_description_t aWorld);


// Returns 1 of the current state is terminal (mine or goal), 0 otherwise 
int check_terminal(int row, int col);


// Returns 1 of the current state is inside map and not blocked) 
int check_valid(const world_description_t* aWorld,int row, int col);

/*  Calculates the reward in the current state.
	-100 if on a mine
	+10 if at the exit
	-1 otherwise 
*/
double calculate_reward(world_description_t aWorld);

/* Calculates and sets the next state, given an action */
void updatePosition(world_description_t *aWorld, int theAction);


/* Prints out the map to the screen */
void print_state();


/*
	world_map is an array that describes the world.

	To read this: the world is a 4 by 8 grid in any position the number 
	corresponds to one of {START, GOAL, FREE, OBSTACLE, MINE}
	
*/


int world_map[7][9] = 
{ 
	{ 1, 1, 1, 1, 1, 1 ,1, 1, 1 }, 
	{ 1, 1, 0, 0, 0, 0, 0, 1, 1 }, 
	{ 1, 0, 0, 1, 0, 1, 0, 0, 1 },
	{ 1, 1, 0, 1, 0, 1 ,0, 1, 1 },
	{ 1, 0, 0, 1, 0, 1 ,0, 3, 1 },
	{ 1, 1, 0, 0, 0, 0 ,0, 1, 1 },
	{ 1, 1, 1, 1, 1, 1 ,1, 1, 1 }
};



/* GLOBAL VARIABLES FOR RL-GLUE methods (global for convenience) */  
static world_description_t the_world;
static observation_t this_observation;
static reward_observation_terminal_t this_reward_observation;


/* Used if a message is sent to the environment to use fixed start states */
static int fixed_start_state=0;
static int start_row=1;
static int start_col=1;
static int start_orientation = 0;

static string task_spec_string =  
"VERSION RL-Glue-3.0 PROBLEMTYPE episodic \
DISCOUNTFACTOR 1 OBSERVATIONS INTS (0 16) \
ACTIONS INTS (0 4)  REWARDS (-100.0 10.0) \
EXTRA Littman89stateGridWorld(C/C++) by Georgios Pierris.";

/*****************************

	RL-Glue Methods 
	
*******************************/

const char* env_init(){    

	the_world.numRows = 7;
	the_world.numCols = 9;

	/* Allocate the observation variable */
	allocateRLStruct(&this_observation,1,0,0);
	/* That is equivalent to:
		 this_observation.numInts     =  1;
		 this_observation.intArray    = (int*)calloc(1,sizeof(int));
		 this_observation.numDoubles  = 0;
		 this_observation.doubleArray = 0;
		 this_observation.numChars    = 0;
		 this_observation.charArray   = 0;
	*/
	/* Setup the reward_observation variable */
	this_reward_observation.observation=&this_observation;
	this_reward_observation.reward=0;
	this_reward_observation.terminal=0;

  return task_spec_string.c_str(); 
}

/* Sets state and returns 1 if valid, 0 if invalid or terminal */
int set_agent_state(int row, int col, int orientation = 0){
	the_world.agentRow=row;
	the_world.agentCol=col;
	the_world.orientation = orientation;
	
	return check_valid(&the_world,row,col) && !check_terminal(row,col); //Valid and terminal are independent of the orientation
}

void set_random_state(){
	int startRow=rand()%7;
	int startCol=rand()%9;
	int startOrientation = rand()%4;

	while(!set_agent_state(startRow,startCol)){
		startRow=rand()%7;
		startCol=rand()%9;
	}
}
/*
	Standard RL-Glue method. Sets an initial state and returns
	the corresponding observation.
*/
const observation_t *env_start()
{ 
	if(fixed_start_state){
        int state_valid=set_agent_state(start_row,start_col, start_orientation);
        if(!state_valid){
            set_random_state();
        }
    }else{
        set_random_state();
    }
    
	this_observation.intArray[0]=calculate_flat_state(the_world);
  	return &this_observation;
}


const reward_observation_terminal_t *env_step(const action_t *this_action)
{
	/* Make sure the action is valid */
	assert(this_action->numInts==1);
	assert(this_action->intArray[0]>=0);
	assert(this_action->intArray[0]<5);

	updatePosition(&the_world,this_action->intArray[0]);
	this_reward_observation.observation->intArray[0] = calculate_flat_state(the_world);
	//std::cout<<"\n\nJust testing... "<<calculate_flat_state(the_world)<<std::endl;
	this_reward_observation.reward = calculate_reward(the_world);
	this_reward_observation.terminal = check_terminal(the_world.agentRow,the_world.agentCol);
	std::cout<<"\nObservation: "<<this_reward_observation.observation->intArray[0]<<std::endl;;
	print_state();

	return &this_reward_observation;
}

void env_cleanup()
{
	clearRLStruct(&this_observation);
}

const char* env_message(const char* _inMessage) {
	/*	Message Description
 	 * 'set-random-start-state'
	 * Action: Set flag to do random starting states (the default)
	 */
  
  string inMessage = _inMessage;

	if(inMessage == "set-random-start-state") {
        fixed_start_state=0;
        return "Message understood.  Using random start state.";
    }
    
	/*	Message Description
 	 * 'set-start-state X Y Orientation'
	 * Action: Set flag to do fixed starting states (row=X, col=Y)
	 */
	if(inMessage.substr(0,15) == "set-start-state"){
	 	{
      string suffix = inMessage.substr(16);
      int row, col, orient; 
      stringstream sstr; 
      sstr << suffix; 
      sstr >> row; 
      sstr >> col;
	  sstr >> orient;
		
			start_row = row; 
			start_col = col;
			start_orientation = orient;

      fixed_start_state=1;

      return "Message understood.  Using fixed start state.";
   	}
	}

	/*	Message Description
 	 * 'print-state'
	 * Action: Print the map and the current agent location
	 */
	if(inMessage == "print-state"){
		print_state();
		return "Message understood.  Printed the state.";
  }

	return "Littman89stateGridWorld(C++) does not respond to that message.";
}


/*****************************

	Helper Methods 
	
*******************************/

int calculate_flat_state(world_description_t aWorld){

	//FIXME: This will not work if a wall exists even if there is not wall block, e.g., if we allowed a single "thin" wall between two valid blocks
	int observationSum = 0; //It will be a 4 bit integer, e.g., 0101 has walls North and South, i.e., at bit 0 and 2. East is bit 1 and West is bit 3	
	//Check for all four possible orientations to see if a valid block exists. If yes then we don't have a wall, if not then there is a wall
	//Check on the right
	if(!check_valid(&aWorld,aWorld.agentRow ,aWorld.agentCol + 1)){
		std::cout<<"Wall Right "<<aWorld.orientation;
		if(aWorld.orientation == 0){
			observationSum += (int)pow(2.0, 1.0);
		}
		else if(aWorld.orientation == 1){
			observationSum += (int)pow(2.0, 0.0);
		}
		else if(aWorld.orientation == 2){
			observationSum += (int)pow(2.0, 3.0);
		}
		else if(aWorld.orientation == 3){
			observationSum += (int)pow(2.0, 2.0);
		}


	}


	//std::cout<<"Check right: "<<observationSum;
	//Check Down
	if(!check_valid(&aWorld,aWorld.agentRow + 1 ,aWorld.agentCol)){
		std::cout<<"Wall Down ";
		if(aWorld.orientation == 0){
			observationSum += (int)pow(2.0, 2.0);
		}
		else if(aWorld.orientation == 1){
			observationSum += (int)pow(2.0, 1.0);
		}
		else if(aWorld.orientation == 2){
			observationSum += (int)pow(2.0, 0.0);
		}
		else if(aWorld.orientation == 3){
			observationSum += (int)pow(2.0, 3.0);
		}

	}
	//std::cout<<"Check Up: "<<observationSum;
	//Check Left
	if(!check_valid(&aWorld,aWorld.agentRow ,aWorld.agentCol - 1)){
		std::cout<<"Wall Left "; 
		if(aWorld.orientation == 0){
			observationSum += (int)pow(2.0, 3.0);
		}
		else if(aWorld.orientation == 1){
			observationSum += (int)pow(2.0, 2.0);
		}
		else if(aWorld.orientation == 2){
			observationSum += (int)pow(2.0, 1.0);
		}
		else if(aWorld.orientation == 3){
			observationSum += (int)pow(2.0, 0.0);
		}

	}
	//Check Up
	if(!check_valid(&aWorld,aWorld.agentRow - 1,aWorld.agentCol)){
		std::cout<<"Wall Up ";
		if(aWorld.orientation == 0){
			observationSum += (int)pow(2.0, 0.0);
		}
		else if(aWorld.orientation == 1){
			observationSum += (int)pow(2.0, 3.0);
		}
		else if(aWorld.orientation == 2){
			observationSum += (int)pow(2.0, 2.0);
		}
		else if(aWorld.orientation == 3){
			observationSum += (int)pow(2.0, 1.0);
		}

	}
		

	return observationSum;
}

int check_terminal(int row, int col){
	if (world_map[row][col] == WORLD_GOAL || world_map[row][col] == WORLD_MINE){    
		return 1;
	}
	return 0;
}

int check_valid(const world_description_t* aWorld, int row, int col){
	int valid=0;
	if(row < aWorld->numRows && row >= 0 && col < aWorld->numCols && col >= 0){
		if(world_map[row][col] != WORLD_OBSTACLE){
			valid=1;
		}
	}
	return valid;
}

double calculate_reward(world_description_t aWorld){
	if(world_map[aWorld.agentRow][aWorld.agentCol] == WORLD_GOAL){
		return 10.0;
	}

	if(world_map[aWorld.agentRow][aWorld.agentCol] == WORLD_MINE){
		return -100.0;
	}
	

	return -1.0;
}

//FIXME: Add transition probabilities for failed actions
void updatePosition(world_description_t *aWorld, int theAction){

	//orientations: North 0
	//				East  1
	//				South 2
	//				West  3 

	//actions:		Nop			0
	//				Forward		1
	//				Turn Left	2
	//				Turn Right  3
	//				Turn Back	4


	// When the move would result in hitting an obstacles,
  	// the agent simply doesn't move 
	int newRow = aWorld->agentRow;
	int newCol = aWorld->agentCol;
	int newOrientation = aWorld->orientation;

	if (theAction == 0) { // No operation
		;//Don't change anything
	}
	if (theAction == 2){  // make a left turn
		std::cout<<"Current Orientation "<<aWorld->orientation<<std::endl;
		newOrientation = (aWorld->orientation - 1 < 0)? 3 : aWorld->orientation-1;
	}
	if (theAction == 3){  //make a right turn
		newOrientation = (aWorld->orientation + 1 > 3)? 0 : aWorld->orientation+1;
	}
	if (theAction == 4){  
		//Make two rights!!!
		newOrientation = (aWorld->orientation + 1 > 3)? 0 : aWorld->orientation+1;
		newOrientation = (newOrientation + 1 > 3)? 0 : newOrientation+1;
	}
	if(theAction == 1){ //Move forward depends on the orientation

		//Check again
		if (aWorld->orientation == 0) { // move up
			newRow = aWorld->agentRow - 1;
		}
		if (aWorld->orientation == 1){  // move right
			newCol = aWorld->agentCol + 1;

		}
		if (aWorld->orientation == 2){  //move down
			newRow = aWorld->agentRow + 1;

		}
		if (aWorld->orientation == 3){  //move left
			newCol = aWorld->agentCol - 1;

		}

	}


	//Check if new position is out of bounds or inside an obstacle 
	if(check_valid(aWorld,newRow,newCol)){
   		aWorld->agentRow = newRow;
   		aWorld->agentCol = newCol;
		aWorld->orientation = newOrientation;
	}
}

void print_state(){
	int row,col;
  char line[1024];
	sprintf(line, "Agent is at: %d,%d",the_world.agentRow,the_world.agentCol);
  cout << line << endl; 
	cout << "Columns:0-10" << endl;
	cout << "Col    ";
	for(col=0;col<9;col++){
		cout << (col%10) << " ";
	}

	for(row=0;row<7;row++){
		cout << endl << "Row: " << row << " ";
		
		for(col=0;col<9;col++){
			if(the_world.agentRow==row && the_world.agentCol==col){
				//Heading North				
				if(the_world.orientation==0){
					cout << "^ ";
				}
				if(the_world.orientation==1){
					cout << "> ";
				}				
				
				if(the_world.orientation==2){
					cout << "v ";
				}				
				
				if(the_world.orientation==3){
					cout << "< ";
				}				
								

			}
			else{
				if(world_map[row][col]==WORLD_GOAL)
					cout << "G "; 
				if(world_map[row][col]==WORLD_MINE)
					cout << "M ";
				if(world_map[row][col]==WORLD_OBSTACLE)
					cout << "* ";
				if(world_map[row][col]==WORLD_FREE)
					cout << "  ";
			}
		}
	}
	cout << endl; 
}

